#/bin/bash

#aur apps 

yay --noconfirm --needed -S \
  7-zip-full \
  abook-configdir \
  ani-cli \
  anki \
  catppuccin-gtk-theme-macchiato \
  etcher-bin \
  hakuneko-desktop \
  htop-vim \
  i3lock-color \
  kcc \
  kjv-apocrypha \
  lf \
  mutt-wizard \
  noto-color-emoji-fontconfig \
  opustags \
  rpi-imager-bin \
  sc-im \
  task-spooler \
  timeshift \
  ueberzug \
  update-grub \
  upwork \
  urlview-xdg-git \
  xidlehook \
  yt-dlp \


  




